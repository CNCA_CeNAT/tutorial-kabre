import threading
import random
import math
import sys

class PiParams:
    def __init__(self, num_of_points):
        self.num_of_points = num_of_points
        self.partial_result = 0

def calc_partial_pi(params):
    count = 0
    for _ in range(params.num_of_points):
        r1 = random.random()
        r2 = random.random()
        if math.hypot(r1, r2) < 1:
            count += 1
    params.partial_result = count

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"Usage: {sys.argv[0]} num_thread num_points")
        sys.exit(1)

    num_threads = int(sys.argv[1])
    num_points = int(sys.argv[2])
    num_points_per_thread = num_points // num_threads

    threads = []
    parameters = []

    for i in range(num_threads):
        params = PiParams(num_points_per_thread)
        parameters.append(params)
        thread = threading.Thread(target=calc_partial_pi, args=(params,))
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    approx_pi = sum(param.partial_result for param in parameters)
    approx_pi /= (num_threads * num_points_per_thread) / 4

    print(f"Result is {approx_pi}, error {abs(math.pi - approx_pi)}")

